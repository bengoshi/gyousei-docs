Aufbau
=======

Meist erläutern Dokumentationen die Installation und Verwendung derjeweiligen Software. Dies soll hier etwas erweitert werden: Die Erfahrung zeigt, dass Buchführung zwar in einem Verein ein wesentlicher Bestandteil ist, hier es aber oft an Erfahrung fehlt. Selbst wer in der betrieblichen Rechnungswesen fit ist, steht zunächst vor dem Themenkomplex Gemeinnützigkeitsrecht.

* Recht und Rechnungswesen
* Dokumentation gyousei
* Entwickler

Hintergründe
------------
Zunächst vorab: Diese Software soll dazu dienen, die in Vereinen übliche Verwaltungsarbeit zu ermöglichen und zu vereinfachen. Dabei wird davon ausgegangen, dass dies auch die Buchhaltung für den Verein umfasst (warum das Sinn macht, später). Das Rechnungswesen hängt immer an nationalen Besonderheiten. Dies trifft bei Vereinen - insbesondere gemeinnützigen - besonders zu. Daher wird hinsichtlich der Dokumentation derzeit gar nicht der Versuch unternommen, diese zu internationalisieren, da der Fokus klar auf der deutschen Rechtslage liegt.
Warum gyousei oder gyosei? Dies ist das japanische Wort (行政) für Verwaltung. Und genau darum geht es hier - einen Verein zu verwalten.
So unterschiedlich viele Vereine ihrem Zweck oder ihrer Kultur nach sind - die Schnittmenge zwischen Hasenzüchterverein und Fußballverein sind größer als die Unterschiede. Gleichwohl bestehen diese. Da der Augenmerk derzeit aber nicht auf einer allgemeinen Vereinsverwaltung liegt, sondern auf einem speziellen Verein (c-base e.V. in Berlin), mag manches darauf zu stark zugeschnitten erscheinen. Abwarten, eine Generalisierung mancher Funktionen steht noch an. Dies betrifft auch die Lokalisierung. Gemeinhin wird damit die Anpassung an eine Sprache gemeint. Das ist hier ähnlich - in jedem Verein heißen die Gremien, die beispielsweise etwas genehmigen müssen, etwas anders. Oder es bedarf für manche Arbeitsschritte gar keiner Zustimmung. Dafür sollen noch Möglichkeiten der Anpassung geschaffen werden.
Braucht man die Buchhaltung bei sich selbst? Ich sage ganz klar ja.
