************************
Recht und Rechnungswesen
************************

Brauche ich wirklich eine Buchhaltung?
######################################

Kurzform: Ja.

Langform:
An der Buchführung führt kein Weg vorbei. Sie ist erforderlich, um als Vorstand [#f1]_ Rechenschaft über die Mittelverwendung gegenüber der Mitgliederversammlung zu geben. Und ob eine Gemeinnützigkeit besteht oder nicht - das Finanzamt verlangt hiernach.
Wer die Finanzen seines Vereins für die Zukunft steuern möchte, kommt nicht ohne eine solide Basis der vergangenen Zahlungsströme aus.
Natürlich unterscheidet sich die Buchhaltung eines kleinen Vereins mit zehn Mitgliedern von einem Verein mit einigen tausenden Mitgliedern. Die Grundprinzipien bleiben aber erst mal gleich. Bevor Du also Bestandteile voreilig als unnötig zur Seite legst, schaue lieber doppelt hin, ob Du sie wirklich nicht benötigst.
Noch ein Wort zum Thema „Bücher führen“. Wer sich bereits mit dem Thema beschäftigte, dem mag hier die Buchführungspflichten aus dem Handelsgesetzbuch in den Kopf kommen. Die meisten Vereine werden jedoch nicht Bilanzierungspflichtig sein. Ohne jetzt an dieser Stelle ins Detail gehen zu wollen - die praktischen Konsequenzen sind geringer als man denkt. Meine Erfahrung zeigt vor allem, dass der Versuch hier möglichst mit einer Schmalspur läuft an irgendeinem Punkt schief läuft. Die Reparatur kostet dann meist mehr Zeit und Geld, als wenn man es gleich richtig angefasst hätte.
 


.. rubric:: ___

.. [#f1] §§ 27 Abs. 3, 666, 259, 260 BGB; Burhoff, Vereinsrecht, 10. Auflage 2018, Rn. 640 f.
.. [#f2] bar.
.. [#f3] ---
