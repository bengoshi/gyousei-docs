ERP
============
Unter der App ERP verbirgt sich der Buchhaltungsteil.


Debit position
Hier können Sollstellungen für die beim jeweiligen Mitglieder hinterlegten Verträge erstellt werden. Das System prüft dabei folgendes:
Ist das Beendigungsdatum kleiner/größer dem Sollstellungsdatum?
Ist das Startdatum kleiner/größer dem Sollstellungsdatum?
Ist das Datum der letzten Sollstellung kleiner/größer dem Sollstellungsdatum?
Die Prüfung erfolg auf Monatsebene. Wird der Austritt zum 5. Februar eingetragen, wird für den Januar noch eine Sollstellung erzeugt, nicht mehr aber für den Februar, selbst wenn die Sollstellung am 4. Februar gezogen wird.

DATEV-Import
Diese Darstellung ist zunächst höchst rudimentär und soll zunächst nur das wichtigste festhalten. Wie muss eine Datei aussehen, damit ich einen DATEV-Export von Buchungssätzen diesen eingelesen bekomme?

Öffne die Datei mit LibreOfficeCalc. Schau Dir erstmal an, ob Du das richtige Encoding ausgewählt hast - steht das Buchungsschlüssel oder ist das „ü“ irgendwelcher Salat. Falls letzteres, probiere beim Encoding „Westeuropäisch (ISO-8859-1) aus.
Lösche die oberste Zeile, so dass nur eine Zeile mit Bezeichnern da ist. Ganz wichtig - es darf nur ein Tabellenblatt geben und dieses muss in „import_journal“ umbenannt werden. Wenn das Blatt nicht so heißt, ist ein Import nicht möglich. Lösche alle Spalten bis auf:
* Umsatz (ohne Soll/Haben-Kz)
* Soll/Haben-Kennzeichen
* Konto
* Gegenkonto (ohne BU-Schlüssel)
* BU-Schlüssel
* Belegdatum
* Belegfeld 1
* Buchungstext

Bei Belegdatum nimmst Du die Spalte „Leistungsdatum“. Speichere die Datei als im xlsx-Format ab. Lade sie hoch und importiere sie, gehe dann auf die Schaltfläche DATEV-Konvertierung. Das Soll-/Habenkennzeichen wird aufgelöst und das Datum jetzt richtig interpretiert.
Der Import ist an dieser Stelle leider noch etwas haklig. Das Problem ist bekannt und wird noch in Angriff genommen.

Nach dem Import solltest Du das machen, was Du bei jeder Finanbuchhaltung tun solltest:
* Vertraue nie der Software, sondern kontrolliere jeden Import!
* Kontrolliere wieviele Buchungen Du in Deiner Tabelle hattest und wieviele im Import gelandet sind.
* Drucke die eine Summen- und Saldenliste für den exportierten Zeitraum und und mache das für den Import. Gleiche diese ab.
* Alles prima? Dann starte den Import.


