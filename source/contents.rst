.. gyousei documentation master file, created by
   sphinx-quickstart on Sun Jul 21 13:26:22 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to gyousei's documentation!
===================================

.. toctree::
   :maxdepth: 2

   index
   accounting
   gyousei
   development
   erp
   memo
   todo

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
